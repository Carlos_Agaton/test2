@extends('test.master')
@section('contenido')
<!-- Slider -->
@include('test.partials.slider')
<!-- Fin del slider -->

<div class="row brown lighten-4" style="margin-bottom: 0px;">
    <div class="col s12 center-align">
        <h4>PREPARE TO EXPERIENCE PURE ADRENALINE</h4>
    </div>
    <div class="container">
        <div class="row">
            <div class="col s12">
                <div class="col s6 m6 l3">
                    <div class="s12">
                        <center><i class="large material-icons green-text">query_builder</i></center>
                    </div>
                    <div class="col s12">
                        <h5 class="center-align green-text">DURATION</h5>
                        <h5 class="center-align">2 Hours</h5>
                    </div>
                </div>
                <div class="col s6 m6 l3">
                    <div class="s12">
                        <center><i class="large material-icons green-text">today</i></center>
                    </div>
                    <div class="col s12">
                        <h5 class="center-align green-text">AVAILABLE</h5>
                        <h5 class="center-align">Everyday</h5>
                    </div>
                </div>
                <div class="col s6 m6 l3">
                    <div class="s12">
                        <center><i class="large material-icons green-text">perm_identity</i></center>
                    </div>
                    <div class="col s12">
                        <h5 class="center-align green-text">CAPACITY</h5>
                        <h5 class="center-align">60 People</h5>
                    </div>
                </div>
                <div class="col s6 m6 l3">
                    <div class="s12">
                        <center><i class="large material-icons green-text">supervisor_account</i></center>
                    </div>
                    <div class="col s12">
                        <h5 class="center-align green-text">ACTIVITY LEVEL</h5>
                        <h5 class="center-align">Medium to High</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row green darken-1">
    <div class="container">
        <div class="col s12">
            <p class="flow-text center-align white-text">
                Fell the wind in you fae and butterflies in your stomach as you fly full speed at 200 meters above the river, smog
                the  treetops, enjoying the majestic scenery surrounding CANOPY RIVER, a place where adventure has only name and
                the special guest is you Ahh! almost forgot...
            </p>
        </div>
        <div class="col s12">
            <p class="flow-text center-align white-text">
                WE WILL CLOSE THIS ADVENTURE ABOARD <br>SOME CUTE AND FLIRTY MULES.
            </p>
        </div>
    </div>
</div>
<div class="row white">
    <div class="container">
        <div class="col s12">
            <div class="col s6 m6 l3">
                <img class="circle responsive-img" src="images/1images.jpg" alt="" style="height: 193px;">
            </div>
            <div class="col s6 m6 l3">
                <img class="circle responsive-img" src="images/2images.jpg"  style="height: 193px;">
            </div>
            <div class="col s6 m6 l3">
                <img class="circle responsive-img" src="images/3images.jpg" alt="" style="height: 193px;">
            </div>
            <div class="col s6 m6 l3">
                <img class="circle responsive-img" src="images/4images.jpg" alt="" style="height: 193px;">
            </div>
        </div>
    </div>
</div>
<div class="row brown lighten-4" style="margin-bottom: 0px;">
    <div class="container">
        <div class="row">
            <div class="col s12 m12 l12">
                <div class="col s12 m12 l4">
                    <div class="col s12 m12 l12">
                        <h4 class="center-align green-text">WHATS INCLUDED</h4>
                        <p>Shuttle Service<i class="left material-icons green-text">stop</i></p>
                        <p>Complimentary welcome drink<i class="left material-icons green-text">stop</i></p>
                        <p>12 Zip lines<i class="left material-icons green-text">stop</i></p>
                        <p>Mule ride (15 min.)<i class="left material-icons green-text">stop</i></p>
                        <p>Purified water during excursione<i class="left material-icons green-text">stop</i></p>
                        <p>Tequila tasting and tour<i class="left material-icons green-text">stop</i></p>
                    </div>
                </div>
                <div class="col s12 m12 l4">
                    <div class="col s12 m12 l12">
                        <h4 class="center-align green-text">WHAT TO WEAR & BRING</h4>
                        <p>Light clothing, confortable shoes<i class="left material-icons green-text">stop</i></p>
                        <p>Insect repellent<i class="left material-icons green-text">stop</i></p>
                        <p>Cameras are not allowed<i class="left material-icons green-text">stop</i></p>
                    </div>
                </div>
                <div class="col s12 m12 l4">
                    <div class="col s12 m12 l12">
                        <h4 class="center-align green-text">DEPARTS FROM</h4>
                        <p>NUEVO VALALRTA
                            <br>(Av. Mexico #570 Junto a glorieta del tigre) Pick up Shedule:
                            <br> 7:50 Hrs. | 8:50 Hrs. | 9:50 Hrs. | 11:50 Hrs. | 13:50 Hrs.
                        </p>
                        <p>Marina Vallarta.
                            <br>(Collage Vallarta) Pick up Shedule:
                            <br> 7:50 Hrs. | 8:50 Hrs. | 9:50 Hrs. | 11:50 Hrs. | 13:50 Hrs.
                        </p>
                        <p>PLAZA LAS GLORIAS
                            <br>(Fco. Medina Ascencio #1989 local H2-A) Pick up Shedule:
                            <br> 7:50 Hrs. | 8:50 Hrs. | 9:50 Hrs. | 11:50 Hrs. | 13:50 Hrs.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row green darken-4" style="margin-bottom: 0px;">
    <br>
    <div class="col s12">
        <div class="col s12 m12 l7">
            <img class="responsive-img" src="images/zonas.jpg" alt="">
        </div>
        <div class="col s12 m12 l5">
            <div class="col s12">
                <h4 class="center-align white-text">TRAVEL DISTANCE</h4>
            </div>
            <div class="col s12">
                <img class="responsive-img" src="images/lista.jpg" alt="">
            </div>
        </div>
    </div>
    <br>
</div>
<div class="row green darken-1" style="margin-bottom: 0px;">
    <div class="container">
        <div class="col s12">
            <h4 class="center-align">RESTRICTIONS</h4>
        </div>
        <div class="col s12">
            <div class="col s12 m6 l4">
                <li class="center-align flow-text white-text">Minimum age: 6  years ald</li>
            </div>
            <div class="col s12 m6 l4">
                <li class="center-align flow-text white-text">Maximum weight: 252 lbs.</li>
            </div>
            <div class="col s12 m6 l4">
                <li class="center-align flow-text white-text">For you own safety, people with asthma, heart deceases and pregmant woman are not allowed on this ride</li>
            </div>
        </div>
    </div>
</div>
<div class="row brown lighten-4">
    <div class="container">
        <div class="row">
            <div class="col s12 m12 l12">
                <div class="col s12 m6 l4">
                    <div class="green circle" style="width: 300px;height: 300px;">
                        <h4 class="center-align white-text">ADULTOS</h4>
                        <h5 class="center-align white-text">From:</h5>
                        <h1 class="center-align white-text">$80 usd</h1>
                    </div>
                    <center><a class="green waves-effect waves-light btn">BOOK NOW</a></center>
                </div>
                <div class="col s12 m6 l4">
                    <div class="green circle" style="width: 300px;height: 300px;">
                        <h4 class="center-align white-text">CHILD</h4>
                        <h5 class="center-align white-text">From:</h5>
                        <h1 class="center-align white-text">$5 usd</h1>
                    </div>
                    <center><a class="green waves-effect waves-light btn">BOOK NOW</a></center>
                </div>
                <br>
            </div>
        </div>
        <div class="divider white"></div>
        <div class="row">
            <div class="col s12">
                <div class="col s12">
                    <br>
                    <h4 class="flow-text center-align">FIND MORE WAYS TO EXPERIENCE THE ADVENTURE</h4>
                </div>
                <div class="row">
                    <div class="col s12 m12 l12">
                        <div class="col s6 m3 l3">
                            <img class="responsive-img" src="images/1.jpg" alt="">
                        </div>
                        <div class="col s6 m3 l3">
                            <img class="responsive-img" src="images/2.jpg" alt="">
                        </div>
                        <div class="col s6 m3 l3">
                            <img class="responsive-img" src="images/3.jpg" alt="">
                        </div>
                        <div class="col s6 m3 l3">
                            <img class="responsive-img" src="images/4.jpg" alt="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col s12 m12 l12">
                        <div class="col s6 m3 l3">
                            <img class="responsive-img" src="images/2.jpg" alt="">
                        </div>
                        <div class="col s6 m3 l3">
                            <img class="responsive-img" src="images/6.jpg" alt="">
                        </div>
                        <div class="col s6 m3 l3">
                            <img class="responsive-img" src="images/6.jpg" alt="">
                        </div>
                        <div class="col s6 m3 l3">
                            <img class="responsive-img" src="images/8.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection