<!--Navbar white-->
<div class="navbar-fixed">
    <nav>
        <div class="nav-wrapper white">
            <a href="#" class="brand-logo"><img src="/images/Canopy_Puerto_Vallarta_Adventures.png" alt=""></a>
            <a href="#" data-activates="mobile-demo" class="button-collapse green-text"><i class="material-icons">menu</i></a>
            <ul class="right hide-on-med-and-down">
                <li><a href="" class="green-text">Call now 1 888 334 8877<i class="material-icons left">call</i></a></li>
                <li><a class="dropdown-button green-text" href="#!" data-activates="dropdown1">Language<i class="material-icons right">arrow_drop_down</i></a></li>
                <li><a href="#" class="green-text">Contact us</a></li>
                <li><img src="images/tripadvisor-xxl.png" width="30px" alt=""></li>
                <li><img src="images/instagram-2048-black.png" width="30px" alt=""></li>
                <li><img src="images/facebook-2.png" width="30px" alt=""></li>
                <li><img src="images/twitter-4096-black.png" width="30px" alt=""></li>
                <li><img src="images/youtube-play-icon-75261.png" width="30px" alt=""></li>

            </ul>
        </div>
    </nav>
</div>
<!-- Fin del Navbar -->
<!--Navbar-->
<div class="navbar-">
    <nav>
        <div class="nav-wrapper white">
            <ul class="right hide-on-med-and-down">
                <li><a href="#axovia" class="green-text">Home</a></li>
                <li><a href="#aventuras" class="green-text">Activities</a></li>
                <li><a href="#salidas" class="green-text">Restaurant</a></li>
                <li><a href="contacto" class="green-text">Gallery</a></li>
                <li><a href="contacto" class="green-text">Blog</a></li>
                <li><a href="contacto" class="green-text">Ejido</a></li>
                <li class="white-text">Espacio</li>
                <li class="white-text">Espacio</li>
                <li class="white-text">Espacio</li>
                <li class="white-text">Espacio</li>
                <li class="white-text">Espacio</li>
                <li class="white-text">Espacio</li>
                <li class="white-text">Espacio</li>
                <li class="white-text">Espacio</li>
            </ul>
            <ul class="side-nav" id="mobile-demo">
                <li><a href="#!" class="green-text">Home</a></li>
                <li><a href="#!" class="green-text">Activities</a></li>
                <li><a href="#!" class="green-text">Restaurant</a></li>
                <li><a href="#!" class="green-text">Gallery</a></li>
                <li><a href="#!" class="green-text">Blog</a></li>
                <li><a href="#!" class="green-text">Ejido</a></li>
                <li><a href="#!">Contacto<i class="material-icons left">call</i></a></li>
            </ul>
        </div>
    </nav>
</div>
<!-- Fin del Navbar -->