<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Axovia</title>
</head>
<body>
<!-- Dropdown Structure -->
<ul id="dropdown1" class="dropdown-content">
    <li><a href="#!">Espanol</a></li>
</ul>

<ul id="dropdown2" class="dropdown-content">
    <li><a href="#!">ZIP LINE</a></li>
    <li><a href="#!">Activity 2</a></li>
    <li><a href="#!">Activity 3</a></li>
</ul>

@include('test.partials.navbar')

@yield('contenido')

<!-- Footer -->
<footer class="page-footer grey lighten-3">
    <div class="footer-copyright">
        <div class="container">
            <b class="black-text text-ligthen-4">� 2015 Copyright Carlos Agaton</b>
        </div>
    </div>
</footer>
<!-- Fin del footer -->
<!--Import jQuery before materialize.js-->
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="js/materialize.min.js"></script>
<!-- SideNav -->
<script>
    $(".button-collapse").sideNav();
</script>
<!-- Slider -->
<script !src="">
    $(document).ready(function(){
        $('.slider').slider(
                {
                    full_width: true,
                    height: 400
                });
    });
</script>
<script>
    $(document).ready(function(){
        $('.scrollspy').scrollSpy();
    });
</script>
<script>
    $('.datepicker').pickadate({
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 15 // Creates a dropdown of 15 years to control year
    });
</script>
@yield('scripts')
</body>
</html>